
// Subscription Widget
var widgetSubscription = {
	widget: $('[data-widget-name="subscription"]'),
	doc: $(document),
	parent: $('[data-subscription-state]'),
	state: 'non-member', // default state
	model: {
		email: '',
		username : ''
	},
	bind: function() {
		this.doc.on('click', '#submitForm', function(event) {
			widgetSubscription.validate();
		});

		this.doc.on('click', '[data-target="isMember"]', function(event) {
			widgetSubscription.update_form_view('member');
		});

		this.doc.on('click', '[data-target="isNonMember"]', function(event) {
			widgetSubscription.update_form_view('non-member');
		});

		this.doc.on('blur', '#inputEmail', function() {
			widgetSubscription.model.email = $(this).val();
		});

		this.doc.on('blur', '#inputUsername', function() {
			widgetSubscription.model.username = $(this).val();
		});

		this.doc.on('keyup', '#inputUsername, #inputEmail', function() {
			if ($(this).val() === '') {
				$('#errorInputs').hide();
			}
		});

	},
	update_form_view: function(state) {
		try {
			if (state === 'member') {
				widgetSubscription.parent.attr('data-subscription-state','member');
			}
			else if (state === 'non-member') {
				widgetSubscription.parent.attr('data-subscription-state','non-member');
			}

			widgetSubscription.state = state;
		} catch(e) {
			console.log(e);
			return false;
		}
	},
	validate_email: function(emailAddress) {
	    var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
	    return pattern.test(emailAddress);
	},
	validate_username: function(username) {
		if (username.indexOf(' ') > 0) {
			return false;
		}
		else {
			return true;
		}
	},
	validate: function() {
		var err = $('#errorInputs');

		if (widgetSubscription.state === 'non-member') {
			if (widgetSubscription.model.email === '') {
				err.show().html('Please enter your email address');
			}
			else {
				if (widgetSubscription.validate_email(widgetSubscription.model.email)) {
					widgetSubscription.submit_form();
					err.hide();
				}
				else {
					err.show().html('Please check email format. <em>e.g. youremail@domain.com</em>');
				}
			}
		}
		else if (widgetSubscription.state === 'member'){
			if (widgetSubscription.model.email === '' || widgetSubscription.model.username === '') {
				err.show().html('Both your email and username are required');
			}
			else {
				if (widgetSubscription.validate_email(widgetSubscription.model.email) && widgetSubscription.validate_username(widgetSubscription.model.username)) {
					widgetSubscription.submit_form();
					err.hide();
				}
				else {
					err.show().html('Please check your username or email');
				}
			}
		}
	},
	show_success: function() {
		$('#successMessage').addClass('in');
	},
	show_error: function() {
		$('#errorInputs').text('Something went wrong while submitting your information');
	},
	processing: function() {
		var template = '<div class="processing text-center"><i class="zmdi zmdi-hc-spin zmdi-settings zmdi-hc-2x"></i><br><span>Processing</span></div>';

		if (widgetSubscription.widget.find('.processing').length > 0) {
			widgetSubscription.widget.find('.processing').remove();
		}
		else {
			widgetSubscription.widget.find('.widget-body').prepend(template);
		}
	},
	submit_form: function() {
		widgetSubscription.processing();

		$.ajax({
			url: 'https://response-test.titan.analogfolk.com',
			type: 'POST',
			data: widgetSubscription.model
		})
		.done(function(data) {
			if (data.success === 'true') {
				widgetSubscription.show_success();
			}
		})
		.fail(function(error) {
			widgetSubscription.show_error();
		})
		.always(function() {
			widgetSubscription.processing();
		});
		
	},
	reset: function() {
		$('#inputEmail, #inputUsername').val('');
	},
	build: function() {
		this.bind();
	}
}


// For demo purpose only
var sampleExtraWidget = {
	build: function() { console.log('extra widget loaded'); }
}


// Something that will run declared widgets on a given page
var widgets = {
	run: function(widgetItems) {
		var arr = widgetItems.widgets;
		var len = arr.length;

		for (var i = 0; i < len; i++) {
			eval(arr[i]).build();
		}
	}
}